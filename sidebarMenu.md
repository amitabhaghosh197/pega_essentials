# Side Bar Menu CSS

```
@media screen and (min-width:1025px){
  .collapse-nav-auto > #sidebar-region-one{
  top:50px;
}
  
  .menu-format-create-case-menu.menu-vertical > .menu-item > .menu-item-anchor, .menu-format-primary-navigation.menu-vertical > .menu-item > .menu-item-anchor, .menu-format-create-case-menu.menu > .menu-item > .menu-item-anchor.menu-slide-back-anchor, .menu-format-primary-navigation.menu > .menu-item > .menu-item-anchor.menu-slide-back-anchor{
   background:#0b2e50; 
  }
  
  .menu-format-primary-navigation.menu-vertical > .menu-item > .menu-item-anchor{
    color: #fff !important;
    font-size:14px;
    font-weight:600;
    border-bottom: 1px solid #1f3e63;
  }
  .menu-format-primary-navigation.menu-vertical > .menu-item > .menu-item-anchor:hover,
  .menu-format-create-case-menu.menu-vertical > .menu-item > .menu-item-anchor:hover{
    background:#ff7000;
  }
  .menu-format-primary-navigation.menu-bar.menu-vertical > .menu-item > .menu-item-anchor .menu-item-title,
  .menu-format-primary-navigation.menu-bar.menu-vertical > .menu-item > .menu-item-anchor .menu-item-icon-imageclass{
    color: #fff !important;
    font-weight: 600;
    font-size: 14px;
  }
  .menu-format-create-case-menu.menu-bar.menu-vertical > .menu-item.menu-item-active > .menu-item-anchor .menu-item-title{
    color: #fff;
    font-size: 14px;
    font-weight: 600;
  }
  
  .menu-format-create-case-menu.menu-bar.menu-vertical > .menu-item.menu-item-active > .menu-item-anchor .menu-item-icon-imageclass{
    color: #fff !important;
  }
  
  .nav-menu-items .layout-outline>.Collapsed>.header-bar:hover{
    background: #ff7000;
  }
  
  .nav-menu-items .collapsible>.header-bar>.header-content>.header-title{
    font-size: 14px;
    font-weight: 600;
    color: #fff;
  }
  
  .nav-menu-items .layout-outline>.Collapsed>.header-bar:focus, .nav-menu-items .layout-outline>.Expanded>.header-bar:focus{
    background: #ff7000;
  }
  .nav-menu-items .layout-outline>.collapsible>.header-bar>.header-content i:before{
    color: #fff;
  }
  
  .menu-format-create-case-menu.menu .menu-item:first-child > .menu-item-anchor .menu-item-title, .menu-format-create-case-menu .menu .menu-item:first-child > .menu-item-anchor .menu-item-title, .menu-format-create-case-menu.menu .menu-item.menu-item-active:first-child > .menu-item-anchor .menu-item-title, .menu-format-create-case-menu .menu .menu-item.menu-item-active:first-child > .menu-item-anchor .menu-item-title{
    font-size: 14px;
    color: #fff;
  }
  
  .menu-format-create-case-menu > li > ul.menu > li:not(:first-child) > a{
    background:#003c73;
  }
  .menu-format-create-case-menu.menu .menu-item.menu-item-active > .menu-item-anchor .menu-item-title, .menu-format-create-case-menu .menu .menu-item.menu-item-active > .menu-item-anchor .menu-item-title{
    font-weight: 600;
  }
  
  .layout-outline-sidebar_gadget h2.header-title,
  aside .dataValueRead .work_grid_item, .screen-layout > aside .dataValueRead .supporting_text{
    color:#fff;
  }
  
  .nav-menu-items div[class*="layout-outline"] > .Collapsed > .header-bar > * > .icon-openclose,
  .nav-menu-items div[class*="layout-outline"] > .Expanded > .header-bar > * > .icon-openclose{
    color:#fff;
  }
  .nav-menu-items .dataValueRead{
    color:#fff !important;
    padding: 4px;
  }
  
  .screen-layout-header_left > .screen-layout-region-main-sidebar1{
  background: #003c73;
}
  
}/* min-width:1025px */


```